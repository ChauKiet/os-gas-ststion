import React from 'react';
import { Router, Scene, Stack } from 'react-native-router-flux';
import Dashboard from "./component/dashboard/index";
import Login from "./component/login/index";
export default function App() {
  return (
    <Router>
      <Scene key="root">
        <Scene
          key="login"
          component={Login}
          hideNavBar={true}
          initial          
        />
        <Scene
          key="home"
          component={Dashboard}
          hideNavBar={true}
          title="Danh sách cột bơm"
          back={false}
        />
      </Scene>
    </Router>
  );
}
