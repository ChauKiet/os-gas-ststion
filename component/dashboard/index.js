import React, { Component, useState } from 'react';
import { Dimensions, AppRegistry, SafeAreaView, StyleSheet, Text, View, AsyncStorage, Button } from 'react-native';
import { Card, Title } from 'react-native-paper';

import { ToggleButton } from 'react-native-paper';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class Dashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            pumps: props.pumps,    //Danh sach cac Pump tu API login
            pumpInfos: [], // Du lieu ban hang cua cac pump
            selectedPump: 0
        }

        AsyncStorage.getItem("Token").then((token) => {
            if (token) {
                this.getLastLogPump(token)
            }
            else {
                console.log("No Token");
            }
        })
    }

    getLastLogPump(Token) {
        if (this.state.pumps == null || this.state.pumps.length == 0) {
            console.error("CHUA CO PUMP NAO");
            return;
        }

        let stationID = this.state.pumps[0].Station_id;
        let apiUrl = `https://cors-anywhere.herokuapp.com/http://seen.cotbomxang.com/api/Mqtt/LastLogPump?StationID=${stationID}`

        fetch(apiUrl, {
            method: 'GET',
            headers: {
                'Token': Token,
            }
        })
            .then((response) => { return response.json() })
            .then((responseJson) => {
                if (responseJson.ErrorCode === 0) {
                    this.setState({ pumpInfos: responseJson.data });
                } else {

                }
            })
    }
    getPumpName(PNum, PType) {
        let type = ["E5", "A95-III", "A95-VI", "DO 0,001S-V", "DO 0,05S-II", "DauHoa"][PType];
        return `${PNum}:${type}`;
    }
    selectStation(event) {
        console.log(event);
        this.setState({ selectedPump: event });

    }


    render() {
        let arrPumpTab = this.state.pumps.map((pump, index) => {
            let name = this.getPumpName(pump.PNum, pump.PType);
            // return (
            // <ToggleButton key={index} value={name} onPress={() => this.selectStation(index)}/>)
            return (<Button key={index} style={styles.btn} title={name} onPress={() => this.selectStation(index)}></Button>);
        })
 
        //thong tin pump dang dc chon
        let pump = this.state.pumps[this.state.selectedPump];
        let pumpInfo = null;
        if (this.state.pumpInfos.length > 0)
            pumpInfo = this.state.pumpInfos.filter(pumpInfo => pumpInfo.MAC = pump.MAC);
        if (pumpInfo != null && pumpInfo.length == 2) {
            pumpInfo = pumpInfo[0];
            // console.error("CO 2 PUMPINFO CUNG 1 MAC ID");
        }
        let currentPumpName = this.getPumpName(pump.PNum, pump.PType);
        
        let LastMoney = 0;
        let   LastLiter= 0;
        let   LastPrice= 0;
        let    LastTotal_EP= 0;
        if (pumpInfo != null) {
            LastMoney = pumpInfo.LastMoney.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            LastLiter = (Math.round(pumpInfo.LastLiter/10)/100).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            LastPrice = pumpInfo.LastPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            LastTotal_EP = (Math.round(pumpInfo.LastTotal_EP/10)/100).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        }
        console.log(LastMoney,LastPrice);
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.listButton}>
                    {arrPumpTab}
                </View>
                <View>
                    {/* <Text> {currentPumpName}</Text>
                    <Text> So Tien :  1000</Text>
                    <Text> So Lit : 123123</Text> */}
                    <Card>
                        <Card.Title style={styles.title} title={currentPumpName} />
                        <Card.Content>
                            <View style={styles.content}>
                                <Title>Tiền</Title>
                                <Title>{LastMoney}</Title>
                            </View>
                            <View style={styles.content}>
                                <Title>Lít</Title>
                                <Title>{LastLiter}</Title>
                            </View>
                            <View style={styles.content}>
                                <Title>Giá</Title>
                                <Title>{LastPrice}</Title>
                            </View>
                            <View style={styles.content}>
                                <Title>Tổng Lít</Title>
                                <Title>{LastTotal_EP}</Title>
                            </View>
                        </Card.Content>
                    </Card>
                </View>
            </SafeAreaView >




        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        maxWidth: '100%',
    },
    listButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 24
    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    btn: {
        height: 40,
        borderBottomWidth: 2,
        borderBottomColor: '#ededed',
    },
    hightlight: {
        height: 40,
        borderBottomWidth: 2,
        borderBottomColor: 'red',
    },
    title: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    content: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center',
       
    }
});