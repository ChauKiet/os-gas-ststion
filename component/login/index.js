import React, { Component } from 'react';
import {
    Alert,
    LayoutAnimation,
    TouchableOpacity,
    Dimensions,
    Image,
    UIManager,
    KeyboardAvoidingView,
    StyleSheet,
    ScrollView,
    Text,
    View,
    AsyncStorage
} from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';
import md5 from "md5";

// import { LinearGradient } from '../../components/LinearGradient';

// Enable LayoutAnimation on Android
UIManager.setLayoutAnimationEnabledExperimental &&
    UIManager.setLayoutAnimationEnabledExperimental(true);

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            selectedType: null,
            username: 'adseen1',
            password: '123456',
            passwordValid: true,
            usernameValid: true,
            errorLogin: '',
        };

        this.setSelectedType = this.setSelectedType.bind(this);
        this.validatePassword = this.validatePassword.bind(this);
        this.signup = this.signup.bind(this);
    }

    signup() {
        this.setState({ errorLogin: "" });

        LayoutAnimation.easeInEaseOut();
        const usernameValid = this.validateUsername();
        const passwordValid = this.validatePassword();
        if (
            passwordValid &&
            usernameValid
        ) {
            this.setState({ isLoading: true });
            setTimeout(() => {
                LayoutAnimation.easeInEaseOut();
                this.setState({ isLoading: false });

                let apiLogin = `http://seen.cotbomxang.com/api/Mqtt/Login?userName=${this.state.username}&passMd5=${md5(this.state.password)}`

                fetch(apiLogin, {
                    method: 'GET'
                })
                    .then((response) => response.json())
                    .then(async (responseJson) => {
                        if (responseJson.ErrorCode === 0) {
                            //redirect to dasboard
                            await AsyncStorage.setItem("Token", responseJson.Token);
                            // console.log("DA LAY DC MANG CAC PUMP", responseJson.data);
                            this.props.navigation.navigate('home', {pumps: responseJson.data})
                        } else {
                            this.setState({ errorLogin: responseJson.msg });
                        }
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            }, 1500);
        }
    }
    validateUsername() {
        const { username } = this.state;
        const usernameValid = username.length > 0;
        LayoutAnimation.easeInEaseOut();
        this.setState({ usernameValid });
        usernameValid || this.usernameInput.shake();
        return usernameValid;
    }
    validatePassword() {
        const { password } = this.state;
        const passwordValid = password.length >= 6;
        LayoutAnimation.easeInEaseOut();
        this.setState({ passwordValid });
        passwordValid || this.passwordInput.shake();
        return passwordValid;
    }
    setSelectedType = (selectedType) =>
        LayoutAnimation.easeInEaseOut() || this.setState({ selectedType });

    render() {
        const {
            isLoading,
            password,
            passwordValid,
            username,
            usernameValid,
            errorLogin,
        } = this.state;

        return (
            <ScrollView keyboardShouldPersistTaps="handled" contentContainerStyle={styles.container}>
                <KeyboardAvoidingView behavior="position" contentContainerStyle={styles.formContainer}>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <Text style={{ color: '#f44336' }}>{errorLogin}</Text>
                        <FormInput
                            refInput={(input) => (this.usernameInput = input)}
                            icon="user"
                            value={username}
                            onChangeText={(username) => this.setState({ username })}
                            placeholder="Tài khoản"
                            returnKeyType="next"
                            errorMessage={
                                usernameValid ? null : "Tài khoản không bỏ trống"
                            }
                            onSubmitEditing={() => {
                                this.validateUsername();
                                this.emailInput.focus();
                            }}
                        />
                        <FormInput
                            refInput={(input) => (this.passwordInput = input)}
                            icon="lock"
                            value={password}
                            onChangeText={(password) => this.setState({ password })}
                            placeholder="Mật khẩu"
                            secureTextEntry
                            returnKeyType="next"
                            errorMessage={
                                passwordValid ? null : 'Vui lòng nhập mật khẩu hơn 6 ký tự trở lên'
                            }
                            onSubmitEditing={() => {
                                this.validatePassword();
                            }}
                        />
                        <Button
                            loading={isLoading}
                            title="Đăng nhập"
                            containerStyle={{ flex: -1 }}
                            buttonStyle={styles.signUpButton}
                            onPress={this.signup}
                        />
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        );
    }
}
export const FormInput = (props) => {
    const { icon, refInput, ...otherProps } = props;
    return (
        <Input
            {...otherProps}
            ref={refInput}
            inputContainerStyle={styles.inputContainer}
            leftIcon={
                <Icon name={icon} type={'simple-line-icon'} color="#7384B4" size={18} />
            }
            inputStyle={styles.inputStyle}
            autoFocus={false}
            autoCapitalize="none"
            keyboardAppearance="dark"
            errorStyle={styles.errorInputStyle}
            autoCorrect={false}
            blurOnSubmit={false}
            placeholderTextColor="#7384B4"
        />
    );
};

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        paddingBottom: 20,
        paddingTop: 20,
        backgroundColor: '#293046',
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    formContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    signUpText: {
        color: 'white',
        fontSize: 28,
        textAlign: 'center'
    },
    whoAreYouText: {
        color: '#7384B4',
        fontSize: 14,
    },
    userTypesContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: SCREEN_WIDTH,
        alignItems: 'center',
    },
    userTypeItemContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.5,
    },
    userTypeItemContainerSelected: {
        opacity: 1,
    },
    userTypeMugshot: {
        margin: 4,
        height: 70,
        width: 70,
    },
    userTypeMugshotSelected: {
        height: 100,
        width: 100,
    },
    userTypeLabel: {
        color: 'yellow',
        fontSize: 11,
    },
    inputContainer: {
        paddingLeft: 8,
        borderRadius: 40,
        borderWidth: 1,
        borderColor: 'rgba(110, 120, 170, 1)',
        height: 45,
        marginVertical: 10,
        width: SCREEN_WIDTH - 40,
    },
    inputStyle: {
        flex: 1,
        marginLeft: 10,
        color: 'white',
        fontSize: 16,
    },
    errorInputStyle: {
        marginTop: 0,
        textAlign: 'center',
        color: '#F44336',
    },
    signUpButtonText: {
        fontSize: 13,
    },
    signUpButton: {
        marginTop: 40,
        width: SCREEN_WIDTH - 40,
        borderRadius: Math.round(45 / 2),
        height: 45,
    },
    loginHereContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    alreadyAccountText: {
        fontSize: 12,
        color: 'white',
    },
    loginHereText: {
        color: '#FF9800',
        fontSize: 12,
    },
});